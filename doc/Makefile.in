srcdir      = @srcdir@
VPATH       = $(srcdir)
top_builddir = @top_builddir@
top_srcdir   = @top_srcdir@

EXTRACTED = Gauche-rfb-refe.texi Gauche-rfb-refj.texi
GENERATED = Makefile 
GOSH = @GOSH@
INSTALL = @INSTALL@
MKINSTDIR = $(top_srcdir)/mkinstalldirs
MAKEINFO = @MAKEINFO@
GZIP_PROGRAM = @GZIP_PROGRAM@

TEXIS = Gauche-rfb-ref.texi

# Manual page destination.  Default @mandir@ doesn't include 'share'
# in the path, so I redefine it---a user can still override it by
# make mandir=wherever
prefix      = @prefix@
datadir     = @datadir@
datarootdir = @datarootdir@
mandir      = @mandir@
infodir     = @infodir@

DESTDIR =

all: info

install: all
	if test ! -d $(DESTDIR)$(infodir); then $(MKINSTDIR) $(DESTDIR)$(infodir); fi
	if test -f Gauche-rfb-refe.info.gz -o -f Gauche-rfb-refj.info.gz; then \
	  for info in *.info*; do \
	    $(INSTALL) -m 444 $$info $(DESTDIR)$(infodir)/; \
	  done; \
	fi

uninstall:
	(cd $(infodir); \
	 if test -f Gauche-rfb-refe.info -o -f Gauche-rfb-refe.info.gz; \
	   then rm -rf Gauche-rfb-refe.*; \
	 fi; \
	 if test -f Gauche-rfb-refj.info -o -f Gauche-rfb-refj.info.gz; \
	   then rm -rf Gauche-rfb-refj.*; \
	 fi)

check :

test :

texi : Gauche-rfb-refe.texi Gauche-rfb-refj.texi

html : Gauche-rfb-refe.html Gauche-rfb-refj.html

htmls : Gauche-rfb-refe_toc.html Gauche-rfb-refj_toc.html

dvi : Gauche-rfb-refe.dvi

pdf : Gauche-rfb-refe.pdf

info : Gauche-rfb-refe.info.gz Gauche-rfb-refj.info.gz

Gauche-rfb-refe.html : Gauche-rfb-refe.texi
	texi2html --number Gauche-rfb-refe.texi

Gauche-rfb-refe_toc.html : Gauche-rfb-refe.texi
	texi2html --split section --number Gauche-rfb-refe.texi

Gauche-rfb-refe.dvi : Gauche-rfb-refe.texi
	texi2dvi Gauche-rfb-refe.texi

Gauche-rfb-refe.pdf : Gauche-rfb-refe.texi
	pdftex Gauche-rfb-refe.texi

Gauche-rfb-refe.texi : $(TEXIS) extract
	$(GOSH) ./extract -en -o Gauche-rfb-refe.texi Gauche-rfb-ref.texi

Gauche-rfb-refe.info.gz : Gauche-rfb-refe.texi
	if test X$(MAKEINFO) != X -a X$(GZIP_PROGRAM) != X; then \
	  env LANG=C $(MAKEINFO) --no-warn Gauche-rfb-refe.texi; \
	  rm -rf Gauche-rfb-refe.info*.gz; \
	  $(GZIP_PROGRAM) Gauche-rfb-refe.info; \
	fi

Gauche-rfb-refj.html : Gauche-rfb-refj.texi
	texi2html --init-file=ja-init.pl --number Gauche-rfb-refj.texi

Gauche-rfb-refj_toc.html : Gauche-rfb-refj.texi
	texi2html --init-file=ja-init.pl --split section --number Gauche-rfb-refj.texi
	for f in Gauche-rfb-refj/Gauche-rfb-refj*.html; do \
	  sed 's/^<body lang="en"/<body lang="ja"/' $$f > $$f.t && mv $$f.t $$f; \
	done

Gauche-rfb-refj.dvi : Gauche-rfb-refj.texi
	texi2dvi Gauche-rfb-refj.texi

Gauche-rfb-refj.pdf : Gauche-rfb-refj.texi
	pdftex Gauche-rfb-refj.texi

Gauche-rfb-refj.texi : $(TEXIS) extract
	$(GOSH) ./extract -jp -o Gauche-rfb-refj.texi Gauche-rfb-ref.texi

Gauche-rfb-refj.info.gz : Gauche-rfb-refj.texi
	if test X$(MAKEINFO) != X -a X$(GZIP_PROGRAM) != X; then \
	  env LANG=C $(MAKEINFO) --no-warn Gauche-rfb-refj.texi; \
	  rm -rf Gauche-rfb-refj.info*.gz; \
	  $(GZIP_PROGRAM) Gauche-rfb-refj.info; \
	fi

clean:
	rm -rf core *~ *.aux *.cl *.cls *.cp *.fn *.fns *.ky *.log *.md *.mds \
	       *.pg *.toc *.tp *.tps *.vr *.vrs *.pdf *.dvi *.info* \
	       $(EXTRACTED)

distclean : clean
	rm -rf $(GENERATED)

maintainer-clean : clean
	rm -f Gauche-rfb-ref*.html Gauche-rfb-ref*.dvi Makefile $(GENERATED)
	rm -rf Gauche-rfb-ref*/
