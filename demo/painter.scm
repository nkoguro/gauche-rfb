(use rfb)

(use srfi-9)
(use gauche.uvector)
(use util.match)

(define width 512)
(define height 512)

;; 2D vector
(define (make-vect x y)
  (cons x y))

(define (xcor-vect v)
  (car v))

(define (ycor-vect v)
  (cdr v))

(define (add-vect v1 v2)
  (make-vect (+ (xcor-vect v1) (xcor-vect v2))
             (+ (ycor-vect v1) (ycor-vect v2))))

(define (sub-vect v1 v2)
  (make-vect (- (xcor-vect v1) (xcor-vect v2))
             (- (ycor-vect v1) (ycor-vect v2))))

(define (scale-vect s v)
  (make-vect (* s (xcor-vect v)) (* s (ycor-vect v))))

;; frame
(define-record-type frame
  (make-frame origin edge1 edge2) frame?
  (origin origin-frame)
  (edge1 edge1-frame)
  (edge2 edge2-frame))

(define (frame-coord-map frame)
  (lambda (v)
    (add-vect (origin-frame frame)
              (add-vect (scale-vect (xcor-vect v)
                                    (edge1-frame frame))
                        (scale-vect (ycor-vect v)
                                    (edge2-frame frame))))))

;; painter
(define (draw-line v1 v2)
  (rfb-line (round->exact (xcor-vect v1))
            (round->exact (ycor-vect v1))
            (round->exact (xcor-vect v2))
            (round->exact (ycor-vect v2))
            'black))

(define (segments->painter segment-list)
  (lambda (frame)
       (for-each (lambda (segment)
                   (let ((p1 (car segment))
                         (p2 (cadr segment)))
                     (draw-line ((frame-coord-map frame) (make-vect (car p1)
                                                                    (cadr p1)))
                                ((frame-coord-map frame) (make-vect (car p2)
                                                                    (cadr p2))))))
                 segment-list)))

(define wave (segments->painter '(((0.00 0.84) (0.16 0.60))
                                  ((0.16 0.60) (0.28 0.65))
                                  ((0.28 0.65) (0.42 0.65))
                                  ((0.42 0.65) (0.36 0.86))
                                  ((0.36 0.86) (0.42 1.00))

                                  ((0.00 0.63) (0.16 0.42))
                                  ((0.16 0.42) (0.30 0.58))
                                  ((0.30 0.58) (0.35 0.49))
                                  ((0.35 0.49) (0.26 0.00))
                                  
                                  ((0.42 0.00) (0.49 0.16))
                                  ((0.49 0.16) (0.58 0.00))

                                  ((0.74 0.00) (0.60 0.47))
                                  ((0.60 0.47) (1.00 0.14))

                                  ((1.00 0.35) (0.74 0.65))
                                  ((0.74 0.65) (0.58 0.65))
                                  ((0.58 0.65) (0.64 0.86))
                                  ((0.64 0.86) (0.58 1.00)))))

(define (pixels->painter pixel-list)
  (lambda (frame)
    (for-each (lambda (segment)
                (let ((p1 (car segment))
                      (c (cadr segment)))
                  (let ((v ((frame-coord-map frame) (make-vect (car p1) (cadr p1)))))
                    (rfb-set-pixel (round->exact (xcor-vect v))
                                   (round->exact (ycor-vect v))
                                   c))))
              pixel-list)))

(define bitmap
  (receive (width height data) (rfb-load-bitmap "./painter/dan.bmp")
    (let ((pixel-list '()))
      (do ((y 0 (+ y 1)))
          ((= y height) (pixels->painter pixel-list))
        (do ((x 0 (+ x 1)))
            ((= x width))
          (let ((c (vector-ref data (+ x (* y width)))))
            (unless (eq? c (rgb 255 255 255))
              (push! pixel-list (list (list (/ x width) (/ y height)) c)))))))))


(define (transform-painter painter origin corner1 corner2)
  (lambda (frame)
    (let* ((m (frame-coord-map frame))
           (new-origin (m origin)))
      (painter (make-frame new-origin
                           (sub-vect (m corner1) new-origin)
                           (sub-vect (m corner2) new-origin))))))

(define (beside painter1 painter2)
  (let ((split-point (make-vect 0.5 0.0)))
    (let ((paint-left (transform-painter painter1
                                         (make-vect 0.0 0.0)
                                         split-point
                                         (make-vect 0.0 1.0)))
          (paint-right (transform-painter painter2
                                          split-point
                                          (make-vect 1.0 0.0)
                                          (make-vect 0.5 1.0))))
      (lambda (frame)
        (paint-left frame)
        (paint-right frame)))))

(define (below painter1 painter2)
  (let ((split-point (make-vect 0.0 0.5)))
    (let ((paint-up (transform-painter painter2
                                       split-point
                                       (make-vect 1.0 0.5)
                                       (make-vect 0.0 1.0)))
          (paint-down (transform-painter painter1
                                         (make-vect 0.0 0.0)
                                         (make-vect 1.0 0.0)
                                         split-point)))
      (lambda (frame)
        (paint-up frame)
        (paint-down frame)))))

(define (right-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (right-split painter (- n 1))))
        (beside painter (below smaller smaller)))))

(define (up-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (up-split painter (- n 1))))
        (below painter (beside smaller smaller)))))

(define (corner-split painter n)
  (if (= n 0)
      painter
      (let ((up (up-split painter (- n 1)))
            (right (right-split painter (- n 1))))
        (let ((top-left (beside up up))
              (bottom-right (below right right))
              (corner (corner-split painter (- n 1))))
          (beside (below painter top-left)
                  (below bottom-right corner))))))

(define (flip-vert painter)
  (transform-painter painter
                     (make-vect 0.0 1.0)
                     (make-vect 1.0 1.0)
                     (make-vect 0.0 0.0)))

(define (flip-horiz painter)
  (transform-painter painter
                     (make-vect 1.0 0.0)
                     (make-vect 0.0 0.0)
                     (make-vect 1.0 1.0)))

(define (square-limit painter n)
  (let ((quarter (corner-split painter n)))
    (let ((half (beside (flip-horiz quarter) quarter)))
      (below (flip-vert half) half))))

;; main
(define (init)
  (gl-clear-color 0.0 0.0 0.0 1.0))

(define (main args)
  (rfb-init 512 512 :port 8080 :title "Painter")

  (rfb-clear 'white)

  (rfb-clear-pointer-event)
  (match-let loop (((id t button-mask x y) (rfb-read-pointer-event #t)))
    (unless (= button-mask 1)
      (loop (rfb-read-pointer-event #t))))
      
  (with-rfb-transaction
   (lambda ()
     ((square-limit wave 1) (make-frame (make-vect 0.0 0.0)
                                        (make-vect 512.0 0.0)
                                        (make-vect 0.0 512.0)))))
  (with-rfb-transaction
   (lambda ()
     (rfb-clear 'white)
     ((square-limit bitmap 1) (make-frame (make-vect 0.0 0.0)
                                          (make-vect 512.0 0.0)
                                          (make-vect 0.0 512.0)))))

  (rfb-close)
  0)


