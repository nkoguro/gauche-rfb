;; -*- coding: utf-8; mode: scheme -*-
;;
;; hwr.scm - Tiny Handwriting Recognition Library
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;

(define-module hwr
  (use rfb)
  (use rfb.util)
  (use math.const)
  (use util.list)
  (use srfi-1)

  (export-all)
  )

(select-module hwr)

(define (encode-angle p1 p2)
  (let ((v (round->exact (* 2 (/ (atan (- (point-y p2) (point-y p1))
                                       (- (point-x p2) (point-x p1)))
                                 pi)))))
    (if (= v -2)
        2
        v)))

(define (distance^2 p1 p2)
  (+ (expt (- (point-x p1) (point-x p2)) 2) (expt (- (point-y p1) (point-y p2)) 2)))

(define (uniq lst)
  (if (null? lst)
      lst
      (reverse (fold (lambda (v result)
                       (if (eq? (car result) v)
                           result
                           (cons v result)))
                     (list (car lst))
                     (cdr lst)))))
(define (point x y)
  (list x y))

(define (point-x p)
  (list-ref p 0))

(define (point-y p)
  (list-ref p 1))
  
(define-class <stroke-context> ()
  ((start-point :init-value #f)
   (sample-point :init-value #f)
   (last-point :init-value #f)
   (angle-list :init-value '(()))))

(define (sample-angle? ctxt point)
  (define distance-threshold 9)
  (with-slots (sample-point) ctxt
    (lambda ()
      (<= distance-threshold (distance^2 sample-point point)))))

(define (stroke! ctxt x y)
  (with-slots ((start-point) (sample-point) (last-point) (angle-list)) ctxt
    (lambda ()
      (let ((p (point x y)))
        (unless (sample-point)
          (set! (sample-point) p)
          (set! (start-point) p))
        (when (sample-angle? ctxt p)
          (set! (angle-list) (cons (cons (encode-angle (sample-point) p)
                                         (car (angle-list)))
                                   (cdr (angle-list))))
          (set! (sample-point) p))
        (set! (last-point) p)))))

(define (release! ctxt x y)
  (with-slots ((start-point) (sample-point) (last-point) (angle-list)) ctxt
    (lambda ()
      (let ((p (point x y)))
        (when (sample-point)
          (set! (angle-list)
                (cons '()
                      (if (sample-angle? ctxt p)
                          (cons (cons (encode-angle (sample-point) p)
                                      (car (angle-list)))
                                (cdr (angle-list)))
                          (angle-list)))))
        (set! (sample-point) #f)
        (set! (last-point) p)))))

(define hwr-dictionary '())

;; result is a string or a function that accepts a start point and an end point.
(define (learn! result . angle-list)
  (set! hwr-dictionary
        (assoc-set! hwr-dictionary angle-list (if (string? result)
                                                  (lambda _
                                                    result)
                                                  result)))
  (fold (lambda (angle-seq lst)
          (let ((partial-lst (append lst (list angle-seq))))
            (unless (assoc-ref hwr-dictionary partial-lst #f)
              (set! hwr-dictionary
                    (assoc-set! hwr-dictionary partial-lst #t)))
            partial-lst))
        '()
        (cdr angle-list)))

(learn! "*" '(1) '(0) '(2))
(learn! "*" '(1) '(1 2) '(0))
(learn! "*" '(1) '(2) '(0 1))
(learn! "*" '(1) '(2 1) '(0))
(learn! "*" '(1) '(2) '(0))

(learn! "=" '(0) '(0))

(learn! "(" '(0 1 2))
(learn! "(" '(1 2))
(learn! ")" '(2 1 0))
(learn! ")" '(2 1))

(learn! "+" '(1) '(0))

(define (0-or-6 start end)
  (define loop-threshold 32)
  (if (<= (distance^2 start end) loop-threshold)
      "0"
      "6"))

(learn! 0-or-6 '(2 -1 0 1 2))
(learn! 0-or-6 '(-1 0 1))
(learn! 0-or-6 '(2 -1 0 1))
(learn! 0-or-6 '(2 -1 0 1 2 1))
(learn! 0-or-6 '(-1 0 1 2))

(learn! "1" '(1))

(learn! "2" '(0 2 1 0 -1))
(learn! "2" '(0 2 1 0))
(learn! "2" '(0 1 0))

(learn! "3" '(2 1 0 1 0))
(learn! "3" '(2 1 0 2 1 0))
(learn! "3" '(2 1 0 2 1 0 -1))

(learn! "4" '(1) '(0 2 1 2))
(learn! "4" '(1) '(0 1))
(learn! "4" '(1) '(0 2))
(learn! "4" '(1) '(0 2 1))
(learn! "4" '(1) '(0 1 2 1))
(learn! "4" '(1) '(0 1 2 1 2))

(learn! "5" '(0) '(2 1 0 1))
(learn! "5" '(0) '(-1 2 1 0 1))
(learn! "5" '(0) '(1 0 1))

(learn! "7" '(1 0))

(learn! "8" '(-1 2 1 0 1 2))
(learn! "8" '(0 -1 2 1 0 2))
(learn! "8" '(-1 0 -1 2 1 0 1 2))
(learn! "8" '(0 -1 2 1 0 1 2))
(learn! "8" '(2 -1 0 -1 2 1 0 1 2))
(learn! "8" '(-1 0 -1 2 0 1 2))
(learn! "8" '(0 -1 2 0 1 2))
(learn! "8" '(-1 0 -1 2 0 2))
(learn! "8" '(2 -1 0 -1 2 1 0 2))
(learn! "8" '(-1 0 -1 2 1 0 1 0 1 2))
(learn! "8" '(-1 0 -1 0 -1 2 1 0 2))
(learn! "8" '(-1 2 0 1 2))
(learn! "8" '(0 -1 2 1 0 1 0 1 2))
(learn! "8" '(0 -1 2 1 2))
(learn! "8" '(-1 2 1 2))

(learn! "9" '(1 0 1 2))
(learn! "9" '(1 2 0 1 2))
(learn! "9" '(1 0 -1 0 1 2))
(learn! "9" '(1 0 2))

;; Returns a recognized string and a status('success, 'failed or 'continue).
;; If a status is 'failed or 'continue, a recognized string is #f.
(define (recognize ctxt)
  (with-slots (start-point last-point angle-list) ctxt
    (lambda ()
      (if (null? angle-list)
          (values #f 'failed)
          (let ((proc (assoc-ref hwr-dictionary
                                 (remove null? (map uniq angle-list)) #f)))
            (cond
             ((procedure? proc)
              (values (proc start-point last-point) 'success))
             (proc
              (values #f 'continue))
             (else
              (values #f 'failed))))))))
    
(provide "hwr")

;; end of file
