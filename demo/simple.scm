;; -*- coding: utf-8; mode: scheme -*-
;;
;; simple.scm - 
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;
(use rfb)

(rfb-init 400 400 :port 8080 :title "Retro")

(rfb-line 0 270 399 270 'yellow)
(rfb-line 0 340 399 340 'yellow)
(rfb-line 0 310 399 310 'yellow)
(rfb-line 0 290 399 290 'yellow)
(rfb-line 0 280 399 280 'yellow)

(rfb-line 200 230 200 399 'yellow)
(rfb-line 200 230 100 399 'yellow)
(rfb-line 200 230 300 399 'yellow)
(rfb-line 200 230 0 350 'yellow)
(rfb-line 200 230 399 350 'yellow)

(rfb-box 0 0 399 269 'black :filled? #t)

(rfb-circle 150 120 100 'green :filled? #t)

(rfb-circle 250 150 100 'blue :filled? #t)

(rfb-circle 200 50 100 'red :filled? #t)

(rfb-close)

;; end of file
