(use rfb)
(use blackboard)
(use srfi-11)
(use util.match)

(define (load-bitmap filename)
  (call-with-values (lambda ()
                      (rfb-load-bitmap filename))
    list))

(define slide-list #f)

(define (main args)
  (rfb-init 512 384 :port 8080 :title "Slide")
  
  (print "Now loading bitmap files ...")
  (set! slide-list (map load-bitmap (list "./slide/slide0.bmp"
                                          "./slide/title0.bmp"
                                          "./slide/slide1.bmp"
                                          "./slide/slide2.bmp"
                                          "./slide/slide3.bmp"
                                          "./slide/slide4.bmp"
                                          "./slide/slide5.bmp"
                                          "./slide/slide6.bmp"
                                          "./slide/slide7.bmp"
                                          "./slide/slide8.bmp"
                                          "./slide/slide9.bmp"
                                          "./slide/slide10.bmp"
                                          "./slide/slide11.bmp")))
  (print "Ready to start RemoteSlide.")

  (with-rfb-transaction
   (lambda ()
     (apply rfb-draw-image 0 0 (car slide-list))))

  (rfb-clear-pointer-event)
  (match-let loop (((id t button-mask x y) (rfb-read-pointer-event #t)))
    (unless (= button-mask 1)
      (loop (rfb-read-pointer-event #t))))

  (rfb-clear (rgb 73 82 83))

  (for-each (lambda (slide)
              (with-rfb-transaction
               (lambda ()
                 (apply rfb-draw-image 0 0 slide)))
              (sys-sleep 1))
            (cdr slide-list))

  (print "Start Blackboard Scheme System.")
  (print "You can write '(', ')', '=', '+', '*' and digits on blackboard.")
  
  (start-black-board! 80 64 60 60 4 'white (rgb 73 82 83)
                      '(371 146 133 238)
                      '(63 332 285 50))
  
  (rfb-close)
  0)

