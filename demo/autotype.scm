(define lst 
  '(("(use rfb)" . 1)
    ("(rfb-init 900 320 \n        :port 8080)" . 10)
    ("(rfb-box 200 20 400 300 \n        'orange :filled? #t)" . 7)
    ("(rfb-circle 600 160 140 \n        'cyan :filled? #t)" . 5)))

(display "\x1b[0;33mgosh> \x1b[m")
(flush)

(for-each (lambda (elem)
            (flush)
            (sys-sleep 1)
            (format #t "~a" (car elem))
            (flush)
            (sys-sleep 1)
            (newline)
            (print (eval (read-from-string (car elem)) (current-module)))
            (display "\x1b[0;33mgosh> \x1b[m")
            (flush)
            (sys-sleep (cdr elem)))
          lst)

(sys-sleep 1000)

