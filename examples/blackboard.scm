;; -*- coding: utf-8; mode: scheme -*-
;;
;; blackboard.scm - The best platform for running Scheme.
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;

(define-module blackboard
  (use rfb)
  (use hwr)
  (use util.match)
  (use util.list)

  (export-all))

(select-module blackboard)

(define dot-size #f)
(define board-x #f)
(define board-y #f)
(define board-width #f)
(define board-height #f)
(define board-dot-color #f)
(define board-background-color #f)
(define board-rect #f)
(define board-eval-rect #f)
(define board-clear-rect #f)

(define (board->graphic-x x)
  (+ board-x (* x dot-size)))

(define (board->graphic-y y)
  (+ board-y (* y dot-size)))

(define (graphic->board-x x)
  (floor->exact (/ (- x board-x) dot-size)))

(define (graphic->board-y y)
  (floor->exact (/ (- y board-y) dot-size)))

(define (board-make-line x1 y1 x2 y2 constructor)
  (let ((d (inexact->exact (round (sqrt (+ (expt (- x2 x1) 2)
                                           (expt (- y2 y1) 2)))))))
    (let loop ((pixel-list '())
               (t1 d))
      (cond
       ((= t1 0)
        (cons (constructor x2 y2) pixel-list))
       (else
        (let* ((t2 (- d t1))
               (x (inexact->exact (round (/ (+ (* t1 x1) (* t2 x2)) d))))
               (y (inexact->exact (round (/ (+ (* t1 y1) (* t2 y2)) d)))))
          (loop (cons (constructor x y) pixel-list) (- t1 1))))))))

(define (board-draw-point x y c)
  (rfb-box (board->graphic-x x) (board->graphic-y y)
           (+ (board->graphic-x x) dot-size -1) (+ (board->graphic-y y) dot-size -1)
           c
           :filled? #t)
  (cons x y))

(define (inside? rect x y)
  (match-let1 (sx sy w h) rect
    (and (<= sx x (+ sx w -1))
         (<= sy y (+ sy h -1)))))

(define (press-board-rect last-dot dot-list ctxt str gx gy cont)
  (let ((x (graphic->board-x gx))
        (y (graphic->board-y gy)))
    (stroke! ctxt x y)
    (cont (cons x y)
          (append dot-list
                  (let ((x0 (if last-dot (car last-dot) x))
                        (y0 (if last-dot (cdr last-dot) y)))
                    (board-make-line x0 y0 x y
                                     (lambda (x y)
                                       (board-draw-point x y board-dot-color)))))
          ctxt
          str)))

(define (release-board-rect last-dot dot-list ctxt str gx gy cont)
  (let ((x (graphic->board-x gx))
        (y (graphic->board-y gy)))
    (cond
     ((null? dot-list)
      (cont #f '() (make <stroke-context>) str))
     (else
      (release! ctxt x y)
      (receive (rec-str status) (recognize ctxt)
        (case status
          ((success)
           (format #t "Recognized: ~a~%" rec-str)
           (cont #f '() (make <stroke-context>) (string-append str rec-str " ")))
          ((failed)
           (with-rfb-transaction
            (lambda ()
              (for-each (lambda (pixel)
                          (board-draw-point (car pixel) (cdr pixel)
                                            board-background-color))
                        dot-list)))
           (format #t "Failed: ~s~%" (slot-ref ctxt 'angle-list))
           (cont #f '() (make <stroke-context>) str))
          ((continue)
           (cont #f dot-list ctxt str))))))))

(define (print-result last-dot dot-list ctxt str result cont)
  (with-rfb-transaction
   (lambda ()
     (board-print result)))
  (cont last-dot dot-list ctxt str))

(define (press-eval-rect last-dot dot-list ctxt str gx gy cont)
  (let ((result (x->string (eval (read-from-string str)
                                 (scheme-report-environment 5)))))
    (format #t "Eval: ~a => ~a~%" str result)
    (press-clear-rect last-dot dot-list ctxt str gx gy
                      (cut print-result <> <> <> <> result cont))))

(define (press-clear-rect last-dot dot-list ctxt str gx gy cont)
  (rfb-box board-x board-y
           (+ board-x (* board-width dot-size) -1)
           (+ board-y (* board-height dot-size) -1)
           board-background-color
           :filled? #t)
  (cont last-dot dot-list ctxt ""))

(define (read-event last-dot dot-list ctxt str)
  (match-let1 (id t mask gx gy) (rfb-read-pointer-event #t)
    (cond
     ((and (= mask 1) (inside? board-eval-rect gx gy))
      (press-eval-rect last-dot dot-list ctxt str gx gy read-event))
     ((and (= mask 1) (inside? board-clear-rect gx gy))
      (press-clear-rect last-dot dot-list ctxt str gx gy read-event))
     ((and (= mask 1) (inside? board-rect gx gy))
      (press-board-rect last-dot dot-list ctxt str gx gy read-event))
     ((and (= mask 0) (inside? board-rect gx gy))
      (release-board-rect last-dot dot-list ctxt str gx gy read-event))
     (else
      (read-event last-dot dot-list ctxt str)))))

(define (start-black-board! b-x b-y b-w b-h d-size d-color bg-color
                            eval-rect clear-rect)
  (set! board-x b-x)
  (set! board-y b-y)
  (set! board-width b-w)
  (set! board-height b-h)
  (set! dot-size d-size)
  (set! board-dot-color d-color)
  (set! board-background-color bg-color)
  (set! board-rect (list b-x b-y (* b-w d-size) (* b-h d-size)))
  (set! board-eval-rect eval-rect)
  (set! board-clear-rect clear-rect)

  (press-clear-rect #f '() (make <stroke-context>) "" 0 0 read-event))

(define character-image-list
  '(("0" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("1" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ @ @ _ _ _ _)
            (_ _ @ _ @ _ _ _ _)
            (_ @ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("2" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ @ _ _)
            (_ _ _ _ _ _ @ _ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ @ _ _ _ _ _ _)
            (_ @ _ _ _ _ _ _ _)
            (_ @ @ @ @ @ @ @ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("3" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ @ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ _ _ _ _ @ _ _)
            (_ _ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("4" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ @ @ _ _ _)
            (_ _ _ _ @ @ _ _ _)
            (_ _ _ @ _ @ _ _ _)
            (_ _ @ _ _ @ _ _ _)
            (_ _ @ _ _ @ _ _ _)
            (_ @ _ _ _ @ _ _ _)
            (_ @ _ _ _ @ _ _ _)
            (@ _ _ _ _ @ _ _ _)
            (@ @ @ @ @ @ @ @ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("5" . ((_ _ _ _ _ _ _ _ _)
            (_ _ @ @ @ @ @ _ _)
            (_ _ @ _ _ _ _ _ _)
            (_ _ @ _ _ _ _ _ _)
            (_ _ @ _ _ _ _ _ _)
            (_ @ _ _ _ _ _ _ _)
            (_ @ _ _ _ _ _ _ _)
            (_ @ _ @ @ @ _ _ _)
            (_ @ @ _ _ _ @ _ _)
            (_ _ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("6" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ _ _)
            (_ @ _ _ _ _ _ _ _)
            (_ @ _ _ _ _ _ _ _)
            (_ @ _ @ @ @ _ _ _)
            (_ @ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("7" . ((_ _ _ _ _ _ _ _ _)
            (@ @ @ @ @ @ @ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ @ _ _)
            (_ _ _ _ _ _ @ _ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("8" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("9" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ @ _ _ _ @ @ _)
            (_ _ _ @ @ @ _ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ @ _ _ _ @ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("#" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ @ _ _ @ _ _)
            (_ _ _ @ _ _ @ _ _)
            (_ _ _ @ _ _ @ _ _)
            (_ _ _ @ _ _ @ _ _)
            (_ @ @ @ @ @ @ @ @)
            (_ _ _ @ _ _ @ _ _)
            (_ _ @ _ _ @ _ _ _)
            (_ _ @ _ _ @ _ _ _)
            (_ _ @ _ _ @ _ _ _)
            (_ _ @ _ _ @ _ _ _)
            (@ @ @ @ @ @ @ @ _)
            (_ _ @ _ _ @ _ _ _)
            (_ @ _ _ @ _ _ _ _)
            (_ @ _ _ @ _ _ _ _)
            (_ @ _ _ @ _ _ _ _)
            (_ @ _ _ @ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("t" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ @ @ @ @ @ @ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ @ _)
            (_ _ _ _ @ @ @ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("f" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ @ @ _ _)
            (_ _ _ _ @ _ _ @ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ @ @ @ @ @ @ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ @ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))

    ("?" . ((_ _ _ _ _ _ _ _ _)
            (_ _ _ @ @ @ _ _ _)
            (_ _ @ _ _ _ @ _ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ @ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ _ @ _)
            (_ _ _ _ _ _ @ _ _)
            (_ _ _ _ _ @ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ @ _ _ _ _)
            (_ _ _ _ _ _ _ _ _)))))

(define (board-draw-character sx sy str)
  (let ((image (assoc-ref character-image-list str
                          (assoc-ref character-image-list "?"))))
    (do ((rows image (cdr rows))
         (y sy (+ y (* dot-size 2))))
        ((null? rows))
      (do ((columns (car rows) (cdr columns))
           (x sx (+ x (* dot-size 2))))
          ((null? columns))
        (when (eq? (car columns) '@)
          (rfb-box x y (+ x (* dot-size 2) -1) (+ y (* dot-size 2) -1)
                   board-dot-color
                   :filled? #t))))))

(define (board-print str)
  (let ((len (string-length str)))
    (cond
     ((= len 0)
      #f)
     ((= len 1)
      (let ((sx (+ board-x
                   (round->exact (- (/ (* dot-size board-width) 2)
                                    (* 4.5 (* dot-size 2))))))
            (sy (+ board-y
                   (round->exact (- (/ (* dot-size board-height) 2)
                                    (* 9 (* dot-size 2)))))))
        (board-draw-character sx sy str)))
     ((= len 2)
      (let ((sx (+ board-x
                   (round->exact (- (/ (* dot-size board-width) 2)
                                    (* 9 (* dot-size 2))))))
            (sy (+ board-y
                   (round->exact (- (/ (* dot-size board-height) 2)
                                    (* 9 (* dot-size 2)))))))
        (board-draw-character sx sy (x->string (string-ref str 0)))
        (board-draw-character (+ sx (* dot-size 2 9)) sy
                              (x->string (string-ref str 1)))))
     ((<= 3 len)
      (board-print "?")))))

(provide "blackboard")

;; (define (main args)
;;   (rfb-init 240 240 :port 8080)

;;   (start-black-board! 0 0 60 60 4 'white 'black '(0 0 10 10) '(0 230 10 10))
;;   0)
  
;; end of file
