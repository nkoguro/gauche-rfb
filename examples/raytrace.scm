;; -*- coding: utf-8; mode: scheme -*-
;;
;; raytrace.scm - 
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;

(use rfb)

(define (sq x) (* x x))

(define (mag x y z)
  (sqrt (+ (sq x) (sq y) (sq z))))

(define (unit-vector x y z)
  (let ((d (mag x y z)))
    (values (/ x d) (/ y d) (/ z d))))

(define-class <point> ()
  ((x :accessor x-of
      :init-keyword :x)
   (y :accessor y-of
      :init-keyword :y)
   (z :accessor z-of
      :init-keyword :z)))

(define (distance p1 p2)
  (mag (- (x-of p1) (x-of p2))
       (- (y-of p1) (y-of p2))
       (- (z-of p1) (z-of p2))))

(define (minroot a b c)
  (if (zero? a)
      (/ (- c) b)
      (let ((disc (- (sq b) (* 4 a c))))
        (if (negative? disc)
            #f
            (let ((discrt (sqrt disc)))
              (min (/ (+ (- b) discrt) (* 2 a))
                   (/ (- (- b) discrt) (* 2 a))))))))

;;-----

(define-class <surface> ()
  ((color :accessor color
          :init-keyword :color)))

(define *world* ())

(define-constant eye (make <point> :x 0 :y 0 :z 200))

(define (tracer . rest)
  (let-optionals* rest ((res 1))
    (let ((inc (/ res)))
      (do ((y -50 (+ y inc)))
          ((< (- 50 y) inc))
        (do ((x -50 (+ x inc)))
            ((< (- 50 x) inc))
          (rfb-set-pixel (x->integer (* res (+ 50 x)))
                         (x->integer (* res (+ 50 y)))
                         (apply rgb (color-at x y))))))))

(define (color-at x y)
  (receive (xr yr zr) (unit-vector (- x (x-of eye))
                                   (- y (y-of eye))
                                   (- 0 (z-of eye)))
    (map (lambda (v)
           (round (* v 255)))
         (sendray eye xr yr zr))))

(define (sendray pt xr yr zr)
  (receive (s int) (first-hit pt xr yr zr)
    (if s
        (map (cut * (lambert s int xr yr zr) <>) (color s))
        '(0 0 0))))

(define (first-hit pt xr yr zr)
  (let ((surface #f)
        (hit #f)
        (dist #f))
    (dolist (s *world*)
      (let ((h (intersect s pt xr yr zr)))
        (when h
          (let ((d (distance h pt)))
            (when (or (not dist) (< d dist))
              (set! surface s)
              (set! hit h)
              (set! dist d))))))
    (values surface hit)))

(define (lambert s int xr yr zr)
  (receive (xn yn zn) (normal s int)
    (max 0 (+ (* xr xn) (* yr yn) (* zr zn)))))

;;-----

(define-class <sphere> (<surface>)
  ((radius :accessor radius
           :init-keyword :radius)
   (center :accessor center
           :init-keyword :center)))

(define (defsphere x y z r c)
  (let ((s (make <sphere>
             :radius r
             :center (make <point> :x x :y y :z z)
             :color c)))
    (push! *world* s)
    s))

(define-method intersect ((s <sphere>) pt xr yr zr)
  (let* ((c (center s))
         (n (minroot (+ (sq xr) (sq yr) (sq zr))
                     (* 2 (+ (* (- (x-of pt) (x-of c)) xr)
                             (* (- (y-of pt) (y-of c)) yr)
                             (* (- (z-of pt) (z-of c)) zr)))
                     (+ (sq (- (x-of pt) (x-of c)))
                        (sq (- (y-of pt) (y-of c)))
                        (sq (- (z-of pt) (z-of c)))
                        (- (sq (radius s)))))))
    (if n
        (make <point>
          :x (+ (x-of pt) (* n xr))
          :y (+ (y-of pt) (* n yr))
          :z (+ (z-of pt) (* n zr)))
        #f)))

(define-method normal ((s <sphere>) pt)
  (let ((c (center s)))
    (unit-vector (- (x-of c) (x-of pt))
                 (- (y-of c) (y-of pt))
                 (- (z-of c) (z-of pt)))))

(define (main args)
  (let-optionals* (cdr args) ((res 1))
    (rfb-init (* (x->integer res) 100) (* (x->integer res) 100)
              :title "raytrace"
              :port 8080)
    (set! *world* '())
    (defsphere 0 -300 -1200 200 '(0.8 0 0))
    (defsphere -80 -150 -1200 200 '(0 0.7 0))
    (defsphere 70 -100 -1200 200 '(0 0 0.9))
    (do ((x -2 (+ x 1)))
        ((> x 2))
      (do ((z 2 (+ z 1)))
          ((> z 7))
        (defsphere (* x 200) 300 (* z -400) 40 '(0.75 0.75 0))))
    (tracer (x->integer res)))
  (print "done")
  (rfb-close)
  0)

  ;; end of file

;; end of file
