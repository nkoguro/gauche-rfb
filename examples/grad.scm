#!/usr/bin/env gosh
;; -*- coding: euc-jp; mode: scheme -*-
;;
;; grad.scm - 
;;
;;   Copyright(C) 2005 by KOGURO, Naoki (naoki@koguro.net)
;;
;;   Permission to use, copy, modify, distribute this software and
;;   accompanying documentation for any purpose is hereby granted,
;;   provided that existing copyright notices are retained in all
;;   copies and that this notice is included verbatim in all
;;   distributions.
;;   This software is provided as is, without express or implied
;;   warranty.  In no circumstances the author(s) shall be liable
;;   for any damages arising out of the use of this software.
;;
;;   $Id: $
;;

(use rfb)

(define (main args)
  (rfb-init 400 400 :port 8080)
  (do ((x 0 (+ x 1)))
      ((= x 400) #f)
    (rfb-line x 0 0 x (rgb (x->integer (* (/ x 399) 255))
                           (x->integer (* (- 1 (/ x 399)) 255))
                           128))
    (rfb-line (- 399 x) 399 399 (- 399 x)
              (rgb 128
                   (x->integer (* (/ x 399) 255))
                   (x->integer (* (- 1 (/ x 399)) 255)))))
  (rfb-close)
  0)

;; end of file
