;; -*- coding: utf-8; mode: scheme -*-
;;
;; lifegame.scm - Conway's Game of Life
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;

(use rfb)
(use math.mt-random)

(define field-width 100)
(define field-height 100)
(define cell-width 4)
(define cell-height 4)

(define (next alive-list)
  (define (get-index x y)
    (+ (modulo x field-width) (* field-width (modulo y field-height))))
  (define (get-neighborhood-list x y)
    (map (lambda (delta)
           (get-index (+ x (car delta)) (+ y (cdr delta))))
         '((-1 . -1) ( 0 . -1) ( 1 . -1)
           (-1 .  0)           ( 1 .  0)
           (-1 .  1) ( 0 .  1) ( 1 .  1))))
  (define (get-x i)
    (modulo i field-width))
  (define (get-y i)
    (x->integer (floor (/ i field-width))))
  (define (num-of-alives field x y)
    (fold (lambda (i sum)
            (if (eq? (vector-ref field i) 'alive)
                (+ sum 1)
                sum))
          0 (get-neighborhood-list x y)))
  (let ((field (make-vector (* field-width field-height) #f))
        (check-field (make-vector (* field-width field-height) #f)))
    (define (survive)
      (fold (lambda (alive new-alive-list)
              (if (memq (num-of-alives field (car alive) (cdr alive)) '(2 3))
                  (begin
                    (vector-set! check-field (get-index (car alive) (cdr alive)) #t)
                    (cons alive new-alive-list))
                  new-alive-list))
            '()
            alive-list))
    (define (birth survive-list)
      (fold (lambda (i new-alive-list)
              (cond
               ((vector-ref check-field i)
                new-alive-list)
               ((= (num-of-alives field (get-x i) (get-y i)) 3)
                (vector-set! check-field i #t)
                (cons (cons (get-x i) (get-y i)) new-alive-list))
               (else
                (vector-set! check-field i #t)
                new-alive-list)))
            survive-list
            (fold (lambda (alive index-list)
                    (append index-list
                            (get-neighborhood-list (car alive) (cdr alive))))
                  '()
                  alive-list)))
    (for-each (lambda (alive)
                (vector-set! field (get-index (car alive) (cdr alive)) 'alive))
              alive-list)
    (birth (survive))))

(define (draw alive-list)
  (with-rfb-transaction
   (lambda ()
     (rfb-clear 'black)
     (for-each (lambda (alive)
                 (let ((x (car alive))
                       (y (cdr alive)))
                   (rfb-box (* x cell-width)
                            (* y cell-height)
                            (+ (* x cell-width) (- cell-width 1))
                            (+ (* y cell-height) (- cell-height 1))
                            'white
                            :filled? #t)))
               alive-list)
     alive-list)))

(define (make-alive-list)
  (let loop ((alive-list '())
             (m (make <mersenne-twister>))
             (num 0))
    (cond
     ((< 1000 num)
      alive-list)
     (else
      (let ((x (mt-random-integer m field-width))
            (y (mt-random-integer m field-height)))
        (if (memq (cons x y) alive-list)
            (loop alive-list m num)
            (loop (cons (cons x y) alive-list) m (+ num 1))))))))

(define (acom)
  '((1 . 0) (3 . 1) (0 . 2) (1 . 2) (4 . 2) (5 . 2) (6 . 2)))

(define (main args)
  (rfb-init (* field-width cell-width)
            (* field-height cell-height)
            :port 8080
            :title "lifegame")
  (let loop ((alive-list (acom)))
    (loop (draw (next alive-list))))
  0)
             
;; end of file
