;; -*- coding: utf-8; mode: scheme -*-
;;
;; rfb.scm - Remote Frame Buffer
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;

(define-module rfb
  (use srfi-1)
  (use srfi-11)
  (use gauche.parameter)
  (use gauche.selector)
  (use gauche.sequence)
  (use math.const)
  (use util.list)
  (use util.match)
  (use util.queue)

  (use rfb.util)
  (use rfb.webserver)
  (use rfb.framebuffer)
  (use rfb.receiver)
  (use rfb.vncserver)
  (use rfb.cmdserver)
  (use binary.io)

  (extend rfb.color)

  (export rfb-init
          rfb-close
          rfb-read-pointer-event
          rfb-clear-pointer-event
          rfb-read-key-event
          rfb-clear-key-event
          rfb-read-cut-text
          rfb-clear-cut-text
          with-rfb-transaction
          rfb-set-pixel
          rfb-line
          rfb-triangle
          rfb-clear
          rfb-circle
          rfb-box
          rfb-draw-row
          rfb-load-bitmap
          rfb-draw-image
          rfb-profiler-start
          rfb-profiler-show)
  )

(select-module rfb)

;;;----------

(define rfb-selector #f)
(define rfb-key-event-queue (make-queue))
(define rfb-pointer-event-queue (make-queue))
(define rfb-cut-text-queue (make-queue))
(define rfb-command-queue (make-parameter #f))
(define rfb-return-queue (make-queue))

(define cmd-out #f)
(define server-pid #f)

(define (start-server width height vnc-port-num title http-port
                      cs-in cs-out sc-in sc-out)
  (let ((selector (make <selector>)))
    (set-signal-handler! SIGPIPE (lambda _ #f))
    (close-input-port sc-in)
    (close-output-port cs-out)
    (let ((fb (make-frame-buffer width height)))
      (vnc-server selector title vnc-port-num fb)
      (cmd-server selector cs-in sc-out fb))
    (when http-port
      (web-server selector http-port vnc-port-num title width height))
    (while #t
      (selector-select selector (if (recv-request?) 0 #f))
      (do-all-recv))))

(define (setup-client pid cs-in cs-out sc-in sc-out)
  (close-input-port cs-in)
  (close-output-port sc-out)
  (set! cmd-out cs-out)
  (set! server-pid pid)
  (set! rfb-selector (make <selector>))
  (selector-add! rfb-selector sc-in
                 (lambda (port flag)
                   (with-port-locking port
                     (lambda ()
                       (while (byte-ready? port)
                         (rfb-eval-event (read port))))))
                 '(r)))

(define (rfb-init width height . args)
  (let-keywords* args ((disp :display 0)
                       (title "Untitled")
                       (port #f))
    (set-signal-handler! SIGCHLD
                         (lambda _
                           (sys-waitpid -1 :nohang #t :untraced #t)))
    (let-values (((cs-in cs-out) (sys-pipe :buffering :full))
                 ((sc-in sc-out) (sys-pipe :buffering :line)))
      (let ((pid (sys-fork)))
        (if (eq? pid 0)
            (start-server width height (vnc-port disp) title port
                          cs-in cs-out sc-in sc-out)
            (setup-client pid cs-in cs-out sc-in sc-out))))))

(define (rfb-close)
  (guard (e ((<system-error> e)
             #t))
         (rfb-send-command 'close)
         (sys-waitpid server-pid :untraced #t)))

(define (rfb-eval-event expr)
  (match expr
    (('key-event servant-id t down-flag key)
     (enqueue! rfb-key-event-queue (list servant-id t down-flag key)))
    (('pointer-event servant-id t button-mask x y)
     (enqueue! rfb-pointer-event-queue (list servant-id t button-mask x y)))
    (('cut-text servant-id text)
     (enqueue! rfb-cut-text-queue (servant-id text)))))
    
(define (rfb-send-command . command-args)
  (if (rfb-command-queue)
      (enqueue! (rfb-command-queue) command-args)
      (begin
        (with-port-locking cmd-out
          (lambda ()
            (cmd-encode-command command-args cmd-out)
            (flush cmd-out))))))

(define (rfb-select blocking?)
  (selector-select rfb-selector (if blocking? #f 0)))

(define (rfb-read-pointer-event blocking?)
  (when (queue-empty? rfb-pointer-event-queue)
    (rfb-select blocking?))
  (if (queue-empty? rfb-pointer-event-queue)
      (if blocking?
          (rfb-read-pointer-event blocking?)
          #f)
      (dequeue! rfb-pointer-event-queue)))

(define (rfb-clear-pointer-event)
  (rfb-select #f)
  (dequeue-all! rfb-pointer-event-queue))

(define (rfb-read-key-event blocking?)
  (when (queue-empty? rfb-key-event-queue)
    (rfb-select blocking?))
  (if (queue-empty? rfb-key-event-queue)
      (if blocking?
          (rfb-read-key-event blocking?)
          #f)
      (dequeue! rfb-key-event-queue)))

(define (rfb-clear-key-event)
  (rfb-select #f)
  (dequeue-all! rfb-key-event-queue))

(define (rfb-read-cut-text blocking?)
  (when (queue-empty? rfb-cut-text-queue)
    (rfb-select blocking?))
  (if (queue-empty? rfb-cut-text-queue)
      (if blocking?
          (rfb-read-cut-text blocking?)
          #f)
      (dequeue! rfb-cut-text-queue)))

(define (rfb-clear-cut-text)
  (rfb-select #f)
  (dequeue-all! rfb-cut-text-queue))

(define (with-rfb-transaction proc)
  (let ((result #f))
    (rfb-send-command 'transaction
                      (parameterize ((rfb-command-queue (make-queue)))
                        (call-with-values proc (lambda lst
                                                 (set! result lst)))
                        (dequeue-all! (rfb-command-queue))))
    (apply values result)))

(define (rfb-set-pixel x y c)
  (rfb-send-command 'set-pixels (list (list x y (color-code c)))))

(define (%line x1 y1 x2 y2 constructor)
  (let ((d (inexact->exact (round (sqrt (+ (expt (- x2 x1) 2)
                                           (expt (- y2 y1) 2)))))))
    (let loop ((pixel-list '())
               (t1 0))
      (cond
       ((= t1 d)
        (cons (constructor x1 y1) pixel-list))
       (else
        (let* ((t2 (- d t1))
               (x (inexact->exact (round (/ (+ (* t1 x1) (* t2 x2)) d))))
               (y (inexact->exact (round (/ (+ (* t1 y1) (* t2 y2)) d)))))
          (loop (cons (constructor x y) pixel-list) (+ t1 1))))))))

(define (rfb-line x1 y1 x2 y2 color)
  (cond
   ((or (= x1 x2) (= y1 y2))
    (%rfb-box-fill (min x1 x2) (min y1 y2) (max x1 x2) (max y1 y2) color))
   (else
    (let ((c (color-code color)))
      (rfb-send-command 'set-pixels
                        (%line x1 y1 x2 y2 (cut list <> <> c)))))))

(define (rfb-triangle x1 y1 x2 y2 x3 y3 color . args)
  (let-keywords* args ((filled? #f))
    ((if filled?
         %rfb-triangle-fill
         %rfb-triangle-boundary)
     x1 y1 x2 y2 x3 y3 color)))

(define (%rfb-triangle-boundary x1 y1 x2 y2 x3 y3 color)
  (let* ((c (color-code color)))
    (with-rfb-transaction
     (lambda ()
       (rfb-line x1 y1 x2 y2 c)
       (rfb-line x2 y2 x3 y3 c)
       (rfb-line x3 y3 x1 y1 c)))))

(define (%rfb-triangle-fill x1 y1 x2 y2 x3 y3 color)
  (let* ((c (color-code color))
         (miny (min y1 y2 y3))
         (buf (make-vector (+ (- (max y1 y2 y3) miny) 1) '()))
         (pset (lambda (x y)
                 (vector-set! buf (- y miny)
                              (cons x (vector-ref buf (- y miny)))))))
    (%line x1 y1 x2 y2 pset)
    (%line x2 y2 x3 y3 pset)
    (%line x3 y3 x1 y1 pset)
    (with-rfb-transaction
     (lambda ()
       (fold (lambda (x-lst y)
               (receive (minx maxx) (apply min&max x-lst)
                 (rfb-line minx y maxx y c))
               (+ y 1))
             miny buf)))))

(define (inside-angle? range mx my x y)
  (define (normalize angle)
    (+ angle (* 2 pi (ceiling (- (/ angle (* 2 pi)))))))
  (if range
      (let-values (((sa ea) (min&max (normalize (car range))
                                     (normalize (cadr range)))))
        (let ((px (- x mx))
              (py (- my y)))
          ((cond
            ((<= (car range) (cadr range))
             identity)
            (else
             not))
           (cond
            ((and (= px 0) (= py 0))
             #t)
            ((and (= px 0) (< py 0))
             (<= sa (* 3 pi/2) ea))
            ((and (= px 0) (< 0 py))
             (<= sa pi/2 ea))
            (else
             (<= sa (let ((t (atan py px)))
                      (if (< t 0)
                          (+ t (* 2 pi))
                          t))
                 ea))))))
      #t))

(define (rfb-circle mx my r color . args)
  (let-keywords args ((filled? #f)
                      (range #f))
    ((if filled?
         %rfb-circle-fill
         %rfb-circle-boundary)
     mx my r color :range range)))

(define (%rfb-circle-boundary mx my r color . args)
  (let-keywords args ((range #f))
    (let ((c (color-code color)))
      (define (make-pen sx sy)
        (let ((prev-x sx) (prev-y sy))
          (lambda (x y)
            (cond
             ((and (and prev-x x) (inside-angle? range mx my prev-x prev-y))
              (rfb-line prev-x prev-y x y c))
             (x
              (rfb-set-pixel x y c))
             (else
              #f))
            (when x
              (set! prev-x x)
              (set! prev-y y)))))
      (define (draw pen x y)
        (if (inside-angle? range mx my x y)
            (pen x y)
            (pen #f #f)))
      (let ((pen1 (make-pen mx (- my r)))
            (pen2 (make-pen mx (- my r)))
            (pen3 (make-pen mx (+ my r)))
            (pen4 (make-pen mx (+ my r))))
        (with-rfb-transaction
         (lambda ()
           (do ((dy (- r) (+ dy 1))
                (dx 0 (round->exact (sqrt (- (expt r 2) (expt dy 2))))))
               ((< 0 dy))
             (draw pen1 (+ mx dx) (+ my dy))
             (draw pen2 (- mx dx) (+ my dy))
             (draw pen3 (- mx dx) (- my dy))
             (draw pen4 (+ mx dx) (- my dy)))))))))
               
(define (%rfb-circle-fill mx my r color . args)
  (let-keywords args ((range #f))
    (let ((c (color-code color)))
      (with-rfb-transaction
       (lambda ()
         (do ((y (- my r) (+ y 1))
              (dx 0 (round->exact (sqrt (- (expt r 2) (expt (- my y) 2))))))
             ((< (+ my r) y))
           (if range
               (do ((x (- mx dx) (+ x 1))
                    (pixel-list '()))
                   ((< (+ mx dx) x)
                    (rfb-send-command 'set-pixels pixel-list))
                 (when (and (inside-angle? range mx my x y))
                   (push! pixel-list (list x y c))))
               (rfb-line (- mx dx) y (+ mx dx) y c))))))))

(define (rfb-clear color)
  (rfb-send-command 'clear (color-code color)))

(define (rfb-box x1 y1 x2 y2 color . args)
  (let-keywords args ((filled? #f))
    ((if filled?
         %rfb-box-fill
         %rfb-box-boundary)
     x1 y1 x2 y2 color)))

(define (%rfb-box-boundary x1 y1 x2 y2 color)
  (let ((c (color-code color)))
    (with-rfb-transaction
     (lambda ()
       (rfb-line x1 y1 x2 y1 c)
       (rfb-line x1 y1 x1 y2 c)
       (rfb-line x2 y1 x2 y2 c)
       (rfb-line x1 y2 x2 y2 c)))))

(define (%rfb-box-fill x1 y1 x2 y2 color)
  (let ((c (color-code color)))
    (rfb-send-command 'box x1 y1 x2 y2 c)))

(define (rfb-draw-row x y colors)
  (rfb-send-command 'draw-row x y (map color-code colors)))

(define (rfb-draw-image x y width height data)
  (let y-loop ((py y)
               (rest (cond
                      ((list? data)
                       data)
                      ((vector? data)
                       (vector->list data))
                      (else
                       (map-to <list> values data)))))
    (unless (null? rest)
      (let x-loop ((i 0)
                   (colors '())
                   (rest rest))
        (cond
         ((= i width)
          (rfb-draw-row x py (reverse colors))
          (y-loop (+ py 1) rest))
         (else
          (x-loop (+ i 1) (cons (car rest) colors) (cdr rest))))))))
  
;;-----
(define-syntax %alist
  (syntax-rules ()
    ((_)
     '())
    ((_ symbol rest ...)
     (cons (cons 'symbol symbol) (%alist rest ...)))))

(define (read-bitmap port)
  (define (%read-bitmap)
    (read-file-header 0))
  (define (read-file-header pos)
    (let* ((file-type (read-binary-uint16 port))
           (file-size (read-binary-uint32 port))
           (reserved1&2 (read-binary-uint32 port))
           (offset (read-binary-uint32 port)))
      (read-information-header (+ pos 14) (%alist file-size offset))))
  (define (read-information-header pos header-alist)
    (define (read-windows)
      (let* ((type 'windows)
             (width (read-binary-sint32 port))
             (height (read-binary-sint32 port))
             (planes (read-binary-uint16 port))
             (bit-count (read-binary-uint16 port))
             (compression (read-binary-uint32 port))
             (size-image (read-binary-uint32 port))
             (x-p/m (read-binary-sint32 port))
             (y-p/m (read-binary-sint32 port))
             (color-used (read-binary-uint32 port))
             (color-important (read-binary-uint32 port)))
        (list (+ pos 40)
              (append (%alist type width height planes bit-count compression
                              size-image x-p/m y-p/m color-used color-important)
                      header-alist))))
    (define (read-os/2)
      (let* ((type 'os/2)
             (width (read-binary-sint32 port))
             (height (read-binary-sint32 port))
             (planes (read-binary-uint16 port))
             (bit-count (read-binary-uint16 port))
             (color-used (expt 2 bit-count))
             (compression 0))
        (list (+ pos 12)
              (append (%alist type width height planes bit-count
                              color-used compression)
                      header-alist))))
    (apply read-palettes (if (= (read-binary-uint32 port) 12)
                             (read-os/2)
                             (read-windows))))
  (define (read-palettes pos header-alist)
    (define (read-palette n)
      (let ((palettes (make-vector n)))
        (dotimes (i n (list (+ pos
                               (* n (if (eq? (assoc-ref header-alist 'type) 'windows)
                                        4
                                        3)))
                            (cut vector-ref palettes <>)))
          (vector-set! palettes i (let* ((blue (read-binary-uint8 port))
                                         (green (read-binary-uint8 port))
                                         (red (read-binary-uint8 port)))
                                    (rgb red green blue)))
          (when (eq? (assoc-ref header-alist 'type) 'windows)
            (read-binary-uint8 port)))))
    (let* ((pos+palettes (case (assoc-ref header-alist 'bit-count)
                           ((1 4 8) (read-palette (assoc-ref header-alist
                                                             'color-used)))
                           (else (list pos identity)))))
      (read-data (list-ref pos+palettes 0)
                 (cons (cons 'palettes (list-ref pos+palettes 1)) header-alist))))
  (define (read-data pos header-alist)
    (let* ((offset (assoc-ref header-alist 'offset))
           (width (assoc-ref header-alist 'width))
           (height (assoc-ref header-alist 'height))
           (palettes (assoc-ref header-alist 'palettes))
           (bit-count (assoc-ref header-alist 'bit-count))
           (line-size (ceiling->exact (/ (* bit-count width) 8)))
           (data (make-vector (* width height))))
      (do ((y (if (<= 0 height)
                  (- height 1)
                  0)
              (+ y (if (<= 0 height)
                       -1
                       1))))
          ((= y (if (<= 0 height) -1 height))
           (values width height data))
        (do ((x 0 (+ x 1)))
            ((= x width) (dotimes (i (modulo (- 4 (modulo line-size 4)) 4))
                           (read-binary-uint8 port)))
          (vector-set! data (+ x (* y width))
                       (palettes (case bit-count
                                   ((1 4) ;; TODO
                                    (error "unsupported"))
                                   ((8)
                                    (read-binary-uint8 port))
                                   ((24)
                                    (let* ((b (read-binary-uint8 port))
                                           (g (read-binary-uint8 port))
                                           (r (read-binary-uint8 port)))
                                      (rgb r g b)))
                                   ((32)
                                    (let* ((b (read-binary-uint8 port))
                                           (g (read-binary-uint8 port))
                                           (r (read-binary-uint8 port))
                                           (_ (read-binary-uint8 port)))
                                      (rgb r g b))))))))))
  (parameterize ((default-endian 'little-endian))
    (%read-bitmap)))

(define (rfb-load-bitmap filename)
  (call-with-input-file filename
    (lambda (in)
      (read-bitmap in))))

(define (rfb-profiler-start)
  (rfb-send-command 'profiler-start))

(define (rfb-profiler-show)
  (rfb-send-command 'profiler-show))

(provide "rfb")

;; end of file
