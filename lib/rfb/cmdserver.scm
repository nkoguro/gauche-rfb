;; -*- coding: utf-8; mode: scheme -*-
;;
;; cmdserver.scm - Command Server
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;
(define-module rfb.cmdserver
  (use srfi-1)
  (use gauche.selector)
  (use gauche.sequence)
  (use util.match)
  (use util.queue)
  (use rfb.framebuffer)
  (use rfb.receiver)
  (use gauche.parameter)
  (use srfi-42)
  (use binary.io)

  (export cmd-server
          cmd-encode-command
          cmd-enqueue-send-request
          cmd-key-event
          cmd-pointer-event
          cmd-cut-text)
  )

(select-module rfb.cmdserver)

(autoload rfb.vncserver send-update-request-all-servants vnc-server-shutdown!)

(define frame-buffer #f)
(define cmd-selector #f)
(define cmd-out #f)

(define bitmap-table (make-hash-table))

(define register-bitmap
  (let ((counter 1))
    (lambda (bitmap)
      (hash-table-put! bitmap-table counter bitmap)
      (begin0
        counter
        (inc! counter)))))

(define (get-bitmap id)
  (if (eq? id 0)
      frame-buffer
      (hash-table-get bitmap-table id)))

(define (cmd-encode-command expr out)
  (define (write-cmd v)
    (write-u32 v out))
  (define (write-num v)
    (write-u32 v out))
  (define (write-point x y)
    (write-s32 x out)
    (write-s32 y out))
  (define (write-color c)
    (write-u32 c out))
  (define (write-pixel x y c)
    (write-point x y)
    (write-color c))
  (parameterize ((default-endian 'little-endian))
    (match expr
      (('close)
       (write-cmd 0))
      (('set-pixels pixel-list)
       (write-cmd 1)
       (write-num (size-of pixel-list))
       (for-each (cut apply write-pixel <>) pixel-list))
      (('box x1 y1 x2 y2 c)
       (write-cmd 2)
       (write-point x1 y1)
       (write-point x2 y2)
       (write-color c))
      (('clear c)
       (write-cmd 3)
       (write-color c))
      (('draw-row x y colors)
       (write-cmd 4)
       (write-point x y)
       (write-num (size-of colors))
       (for-each write-color colors))
      (('transaction cmd-list)
       (write-cmd 5)
       (write-num (size-of cmd-list))
       (for-each (cut cmd-encode-command <> out) cmd-list))
      (('profiler-start)
       (write-cmd 6))
      (('profiler-show)
       (write-cmd 7)))))

(define (cmd-eval-command in update)
  (define (read-cmd)
    (read-u32 in))
  (define (read-num)
    (read-u32 in))
  (define (read-point)
    (let* ((x (read-s32 in))
           (y (read-s32 in)))
      (list x y)))
  (define (read-color)
    (read-u32 in))
  (define (read-pixel)
    (append (read-point) (list (read-color))))
  (define cmd-table
    (list->vector
     (list vnc-server-shutdown!
           (lambda ()
             (let* ((n (read-num)))
               (for-each (lambda (pixel)
                           (apply fb-set-pixel! frame-buffer pixel))
                         (list-ec (: i n) (read-pixel))))
             (update))
           (lambda ()
             (let* ((v1 (read-point))
                    (v2 (read-point))
                    (c (read-color)))
               (apply fb-draw-box! frame-buffer (append v1 v2 (list c))))
             (update))
           (lambda ()
             (let* ((c (read-color)))
               (fb-clear! frame-buffer c))
             (update))
           (lambda ()
             (let* ((v (read-point))
                    (n (read-num)))
               (fb-draw-row! frame-buffer (list-ref v 0) (list-ref v 1)
                             (list-ec (: i n) (read-color))))
             (update))
           (lambda ()
             (let* ((n (read-num)))
               (dotimes (i n)
                 (cmd-eval-command in (lambda () #f))))
             (update))
           (lambda ()
             (profiler-start))
           (lambda ()
             (profiler-show)))))
  ((vector-ref cmd-table (read-num) (cut error "Unknown command"))))

(define (cmd-key-event servant-id down-flag key out)
  (write `(key-event ,servant-id ,(sys-time) ,down-flag ,key) out))

(define (cmd-pointer-event servant-id button-mask x y out)
  (write `(pointer-event ,servant-id ,(sys-time) ,button-mask ,x ,y) out))

(define (cmd-cut-text servant-id text out)
  (write `(cut-text ,servant-id ,text) out))

(define cmd-send-queue (make-queue))

(define (cmd-server selector in out fb)
  (set! cmd-selector selector)
  (set! cmd-out out)
  (set! frame-buffer fb)
  (selector-add! selector in
                 (lambda (port flag)
                   (cmd-enqueue-recv-request port))
                 '(r)))

(define (cmd-recv-data in)
  (with-port-locking in
    (lambda ()
      (when (byte-ready? in)
        (cmd-eval-command in send-update-request-all-servants)
        (cmd-enqueue-recv-request in)))))

(define (cmd-enqueue-recv-request in)
  (recv-enqueue! (cut cmd-recv-data in)))

(define (cmd-send-data out)
  (with-port-locking out
    (lambda ()
      (cond
       ((queue-empty? cmd-send-queue)
        (selector-delete! cmd-selector out #f #f))
       (else
        (let ((send-request-proc (dequeue! cmd-send-queue)))
          (send-request-proc out)
          (flush out)))))))

(define (cmd-enqueue-send-request send-request-proc)
  (when (queue-empty? cmd-send-queue)
    (selector-add! cmd-selector cmd-out
                   (lambda (port flag)
                     (cmd-send-data port))
                   '(w)))
  (enqueue! cmd-send-queue send-request-proc))

(provide "rfb/cmdserver")

;; end of file
