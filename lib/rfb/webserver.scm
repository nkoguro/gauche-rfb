;; -*- coding: utf-8; mode: scheme -*-
;;
;; webserver.scm - Tiny Web Server
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;
(define-module rfb.webserver
  (use file.util)
  (use gauche.net)
  (use gauche.selector)
  (use gauche.uvector)
  (use rfc.uri)
  (use srfi-1)
  (use srfi-13)
  (use text.html-lite)
  (use text.tree)
  (use util.list)

  (export web-server)
  )

(select-module rfb.webserver)

(define swf-path (or (find-file-in-paths "FVNC.swf"
                                         :paths (map (cut build-path <> "rfb")
                                                     *load-path*)
                                         :pred file-is-readable?)
                     (error "FVNC.swf not found.")))

(define (read-request in)
  (define (read-request-line in)
    (let ((line (read-line in)))
      (if (eof-object? line)
          '()
          (rxmatch-let (#/([^ ]+) ([^ ]+) HTTP\/(\d+\.\d)/ line)
              (_ method path http-version)
            (read-message-headers in `((method . ,method)
                                       (path . ,path)
                                       (version . ,http-version)))))))
  (define (read-message-headers in req-alist)
    (let loop ((alist req-alist)
               (line (read-line in)))
      (if (or (eof-object? line) (= (string-length line) 0))
          (read-body in alist)
          (rxmatch-let (#/([^:\s]+):\s*(.*)/ line)
              (_ name value)
            (loop (alist-cons (string->symbol (string-downcase name))
                              value
                              alist)
                  (read-line in))))))
  (define (read-body in req-alist)
    (and-let* ((pair (assoc 'content-length req-alist)))
      (read-block (x->integer (cdr pair)) in))
    req-alist)
  (read-request-line in))

(define (write-response out req-alist resp-alist data)
  (format out "HTTP/~a ~a ~a\r\n"
          (assoc-ref req-alist 'version)
          (assoc-ref resp-alist 'status)
          (assoc-ref resp-alist 'reason))
  (let ((byte-data (coerce-to <u8vector> data)))
    (for-each (lambda (p)
                (format out "~a: ~a\r\n" (car p) (cdr p)))
              `(("Connection" . "close")
                ("Content-Length" . ,(size-of byte-data))
                ("Content-Type" . ,(assoc-ref resp-alist 'content-type))))
    (display "\r\n" out)
    (write-block byte-data out)))

(define (return-html vnc-port-num title width height host http-port)
  (let ((flash-vars (format "hostname=~a&port=~a&httpport=~a"
                            (uri-encode-string host)
                            vnc-port-num
                            http-port)))
    (list '((status . 200)
            (reason . "OK")
            (content-type . "text/html"))
          (string-join
           (list
            "<html lang=\"en\">"
            "<head>"
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
            #`"<title>,|title|</title>"
            "</head>"
            "<body>"
            "<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\""
            #`"width=\",width\" height=\",height\""
            "codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab\">"
            "<param name=\"movie\" value=\"FVNC.swf\">"
            "<param name=\"quality\" value=\"high\">"
            "<param name=\"allowScriptAccess\" value=\"sameDomain\">"
            "<param name=\"play\" value=\"true\">"
            "<param name=\"allowScriptAccess\" value=\"sameDomain\">"
            #`"<param name=\"flashVars\" value=\",|flash-vars|\">"
            #`"<embed src=\"FVNC.swf\" width=\",width\" height=\",height\""
            "play=\"true\" quality=\"high\" allowScriptAccess=\"sameDomain\""
            "type=\"application/x-shockwave-flash\""
            #`"flashVars=\",|flash-vars|\""
            "pluginspage=\"http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash\">"
            "</embed>"
            "</object>"
            "</body>"
            "</html>")
           "\r\n"))))

(define (return-swf)
  (list '((status . 200)
          (reason . "OK")
          (content-type . "application/x-shockwave-flash"))
        (call-with-input-file swf-path
          (lambda (in)
            (list->u8vector (port-fold-right cons '() (cut read-byte in)))))))

(define (return-error)
  (list '((status . 501)
          (reason . "Not Implemeted")
          (content-type . "text/html"))
        (tree->string (html:html :lang "en"
                                 (html:head
                                  (html:meta :http-equiv "Content-Type"
                                             :content "text/html; charset=utf-8")
                                  (html:title "Not Implemented"))
                                 (html:body "501 Not Implemented")))))

(define (return-cross-domain-policy vnc-port-num in out)
  (let ((req-str (read-block (string-length "<policy-file-request/>\0") in)))
    (when (string=? req-str #*"<policy-file-request/>\0")
      (format out "<cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"~a\"/></cross-domain-policy>\0" vnc-port-num)
      (flush out))))

(define (web-server selector port vnc-port-num title width height)
  (let ((sock (make-server-socket 'inet port :reuse-addr? #t)))
    (selector-add! selector (socket-fd sock)
                   (lambda _
                     (let* ((client (socket-accept sock))
                            (http-in (socket-input-port client :buffering :full))
                            (http-out (socket-output-port client)))
                       (if (= (peek-byte http-in) 60)
                           (return-cross-domain-policy vnc-port-num http-in http-out)
                           (let ((req-alist (read-request http-in)))
                             (apply write-response http-out req-alist
                                    (cond
                                     ((and (equal? (assoc-ref req-alist 'method)
                                                   "GET")
                                           (equal? (assoc-ref req-alist 'path)
                                                   "/")
                                           (assoc-ref req-alist 'host #f))
                                      (return-html vnc-port-num title width height
                                                   (regexp-replace #/:\d+/
                                                                   (assoc-ref req-alist
                                                                              'host) "")
                                                   port))
                                     ((and (equal? (assoc-ref req-alist 'method)
                                                   "GET")
                                           (equal? (assoc-ref req-alist 'path)
                                                   "/FVNC.swf"))
                                      (return-swf))
                                     (else
                                      (return-error))))))
                       (socket-close client)))
                   '(r))))

(provide "rfb/webserver")

;; end of file
