;; -*- coding: utf-8; mode: scheme -*-
;;
;; framebuffer.scm - Frame Buffer
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;
(define-module rfb.framebuffer
  (use srfi-1)
  (use srfi-11)
  (use gauche.collection)
  (use util.combinations)
  (use rfb.util)
  (use binary.io)
  (use gauche.parameter)
  (use util.list)
  (use rfb.color)

  (export x-of
          y-of
          width-of
          height-of
          buffer-of
          tile-rre-mode?
          background-color
          tile-num-of-boxes
          tile-box-list

          make-frame-buffer
          fb-last-update-time
          fb-updated-tile-list
          fb-all-tiles
          fb-set-pixel!
          fb-set-tiles!
          fb-draw-row!
          fb-draw-box!
          fb-clear!)
  )

(select-module rfb.framebuffer)

(define (inside? obj x y)
  (with-slots (width height) obj
    (lambda ()
      (and (<= 0 x (- width 1))
           (<= 0 y (- height 1))))))

(define-class <tile> ()
  ((frame-buffer :init-keyword :frame-buffer)
   (x :init-keyword :x
      :accessor x-of)
   (y :init-keyword :y
      :accessor y-of)
   (width :init-keyword :width
          :accessor width-of)
   (height :init-keyword :height
           :accessor height-of)
   (buffer :init-keyword :buffer
           :accessor buffer-of)
   (next :init-value #f)
   (prev :init-value #f)
   (background-color :init-value 0
                     :accessor background-color)
   (box-list :init-value '())
   (box-only? :init-value #t)
   (update-time :init-form (sys-time))))

(define (make-tile fb x y width height)
  (make <tile>
    :frame-buffer fb
    :x x
    :y y
    :width width
    :height height
    :buffer (make-vector (* width height) 0)))

(define (tile-num-of-boxes tile)
  (if (tile-rre-mode? tile)
      (length (slot-ref tile 'box-list))
      (error "tile-num-of-boxes can't be used in raw-mode")))

(define (tile-box-list tile)
  (if (tile-rre-mode? tile)
      (reverse (slot-ref tile 'box-list))
      (error "tile-box-list can't be used in raw-mode")))

(define (tile-set-raw-mode! tile)
  (slot-set! tile 'box-only? #f))

(define (tile-set-rre-mode! tile)
  (slot-set! tile 'box-only? #t)
  (slot-set! tile 'box-list '()))

(define (tile-rre-mode? tile)
  (slot-ref tile 'box-only?))

(define (tile-set-pixels! tile pixel-list)
  (let ((updated? #f))
    (for-each (lambda (pixel)
                (let* ((index (+ (* (list-ref pixel 1) (width-of tile))
                                 (list-ref pixel 0)))
                       (prev-color (vector-ref (buffer-of tile) index))
                       (c (list-ref pixel 2)))
                  (unless (equal? prev-color c)
                    (vector-set! (buffer-of tile) index c)
                    (set! updated? #t))))
              pixel-list)
    (when updated?
      (tile-set-raw-mode! tile)
      (tile-update! tile))))

(define (tile-draw-box! tile x y width height c)
  (if (and (tile-rre-mode? tile) (< (tile-num-of-boxes tile) 85))
      (let ((box-list (slot-ref tile 'box-list)))
        (do ((py y (+ py 1)))
            ((<= (+ y height) py))
          (do ((px x (+ px 1)))
              ((<= (+ x width) px))
            (vector-set! (buffer-of tile) (+ px (* (width-of tile) py)) c)))
        (cond
         ((and (= x 0) (= y 0)
               (= width (width-of tile)) (= height (height-of tile)))
          (slot-set! tile 'box-list '())
          (set! (background-color tile) c))
         (else
          (slot-set! tile 'box-list (cons (list x y width height c) box-list))
          (tile-compact-boxes! tile)))
        (tile-update! tile))
      (tile-set-pixels! tile (cartesian-product (list (iota width x)
                                                      (iota height y)
                                                      (list c))))))

(define (tile-compact-boxes! tile)
  (define (combine-box-list box-list)
    (if (< (length box-list) 2)
        box-list
        (let-values (((x1 y1 w1 h1 c1) (apply values (car box-list)))
                     ((x2 y2 w2 h2 c2) (apply values (cadr box-list))))
          (cond
           ((not (= c1 c2))
            box-list)
           ((and (= y1 y2) (= h1 h2) (= (+ x1 w1) x2))
            (combine-box-list (cons (list x1 y1 (+ w1 w2) h1 c1) (cddr box-list))))
           ((and (= y1 y2) (= h1 h2) (= (+ x2 w2) x1))
            (combine-box-list (cons (list x2 y1 (+ w1 w2) h1 c1) (cddr box-list))))
           ((and (= x1 x2) (= w1 w2) (= (+ y1 h1) y2))
            (combine-box-list (cons (list x1 y1 w1 (+ h1 h2) c1) (cddr box-list))))
           ((and (= x1 x2) (= w1 w2) (= (+ y2 h2) y1))
            (combine-box-list (cons (list x1 y2 w1 (+ h1 h2) c1) (cddr box-list))))
           (else
            box-list)))))
  (if (tile-rre-mode? tile)
      (combine-box-list (slot-ref tile 'box-list))
      (error "tile-compact-boxes! can't be used in raw-mode")))
        
(define (tile-clear! tile c)
  (unless (and (tile-rre-mode? tile)
               (eq? (background-color tile) c)
               (eq? (length (slot-ref tile 'box-list)) 0))
    (vector-fill! (buffer-of tile) c)
    (slot-set! tile 'box-only? #t)
    (set! (background-color tile) c)
    (slot-set! tile 'box-list '())
    (tile-update! tile)))
  
(define (tile-update! tile)
  (slot-set! tile 'update-time (sys-time))
  (let* ((fb (slot-ref tile 'frame-buffer))
         (ru-tile (slot-ref fb 'recently-updated-tile))
         (next (slot-ref tile 'next))
         (prev (slot-ref tile 'prev)))
    (unless (eq? tile ru-tile)
      (when next
        (slot-set! next 'prev prev))
      (when prev
        (slot-set! prev 'next next))
      (slot-set! tile 'prev #f)
      (slot-set! tile 'next ru-tile)
      (slot-set! ru-tile 'prev tile)
      (slot-set! fb 'recently-updated-tile tile))))

(define-class <frame-buffer> ()
  ((width :init-keyword :width
          :accessor width-of)
   (height :init-keyword :height
           :accessor height-of)
   (x-num-of-tiles :accessor x-tiles-of)
   (recently-updated-tile)
   (tiles :accessor tiles-of)))

(define-method initialize ((self <frame-buffer>) . args)
  (next-method)
  (let ((width (slot-ref self 'width))
        (height (slot-ref self 'height))
        (tile-list '()))
    (slot-set! self 'x-num-of-tiles
               (+ (quotient width 16) (min 1 (remainder width 16))))
    (do ((y 0 (+ y 16)))
        ((<= height y)
         (slot-set! self 'tiles (list->vector (reverse tile-list)))
         (slot-set! self 'recently-updated-tile (car tile-list))
         (fold (lambda (current prev)
                 (slot-set! prev 'next current)
                 (slot-set! current 'prev prev)
                 current)
               (car tile-list)
               (cdr tile-list)))
      (do ((x 0 (+ x 16)))
          ((<= width x) #f)
        (push! tile-list
               (make-tile self x y (min 16 (- width x)) (min 16 (- height y))))))))

(define (make-frame-buffer width height)
  (make <frame-buffer> :width width :height height))

(define (fb-last-update-time fb)
  (ref* fb 'recently-updated-tile 'update-time))

(define (fb-updated-tile-list fb t)
  (let loop ((lst '())
             (tile (slot-ref fb 'recently-updated-tile)))
    (cond
     ((not tile)
      lst)
     ((< (slot-ref tile 'update-time) t)
      lst)
     (else
      (loop (cons tile lst) (ref tile 'next))))))

(define (fb-all-tiles fb)
  (slot-ref fb 'tiles))

(define (fb-get-tile fb tx ty)
  (let ((i (+ (* ty (x-tiles-of fb)) tx)))
    (if (<= 0 i (- (vector-length (slot-ref fb 'tiles)) 1))
        (vector-ref (tiles-of fb) i)
        #f)))

(define (fb-set-pixel! fb x y c)
  (when (inside? fb x y)
    (let ((tx (ash x -4))
          (ty (ash y -4))
          (dx (logand x 15))
          (dy (logand y 15)))
      (and-let* ((tile (fb-get-tile fb tx ty)))
        (tile-set-pixels! tile (list (list dx dy c)))))))

(define (fb-get-pixel fb x y . opts)
  (let-optionals* opts ((fallback #f))
    (if (inside? fb x y)
        (and-let* ((tile (fb-get-tile fb (ash x -4) (ash y -4))))
          (vector-ref (buffer-of tile)
                      (calc-index (logand x 16) (logand y 16) (width-of tile))))
        (or fallback
            (errorf "fb-get-pixel coordinates out of range (~a, ~a)" x y)))))

(define (fb-draw-row! fb x y colors)
  (fold (lambda (c px)
          (fb-set-pixel! fb px y c)
          (+ px 1))
        x
        colors))
      
(define (fb-draw-box! fb x1 y1 x2 y2 c)
  (define (intersection x1-1 y1-1 x1-2 y1-2
                        x2-1 y2-1 x2-2 y2-2) 
    (let ((x1 (max x1-1 x2-1))
          (y1 (max y1-1 y2-1))
          (x2 (min x1-2 x2-2))
          (y2 (min y1-2 y2-2)))
      (list x1 y1 x2 y2)))
  (receive (sx sy ex ey) (apply values (intersection x1 y1 x2 y2
                                                     0 0
                                                     (- (slot-ref fb 'width) 1)
                                                     (- (slot-ref fb 'height) 1)))
    (when (and (<= sx ex) (<= sy ey))
      (do ((ty (ash sy -4) (+ ty 1)))
          ((< (ash ey -4) ty))
        (do ((tx (ash sx -4) (+ tx 1)))
            ((< (ash ex -4) tx))
          (and-let* ((tile (fb-get-tile fb tx ty)))
            (let-values (((ix1 iy1 ix2 iy2)
                          (apply values (intersection (x-of tile) (y-of tile)
                                                      (+ (x-of tile)
                                                         (width-of tile)
                                                         -1)
                                                      (+ (y-of tile)
                                                         (height-of tile)
                                                         -1)
                                                      x1 y1 x2 y2))))
              (tile-draw-box! tile
                              (- ix1 (x-of tile))
                              (- iy1 (y-of tile))
                              (+ (- ix2 ix1) 1) (+ (- iy2 iy1) 1) c))))))))

(define (fb-clear! fb c)
  (for-each (cut tile-clear! <> c) (tiles-of fb)))

(provide "rfb/framebuffer")

;; end of file
