;; -*- coding: utf-8; mode: scheme -*-
;;
;; vncserver.scm - VNC Server
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;
(define-module rfb.vncserver
  (use binary.io)
  (use binary.pack)
  (use gauche.net)
  (use gauche.selector)
  (use gauche.sequence)
  (use gauche.uvector)
  (use util.list)
  (use util.queue)
  (use rfb.framebuffer)
  (use rfb.receiver)

  (export vnc-port
          vnc-server
          vnc-server-shutdown!
          send-update-request-all-servants)
  )

(select-module rfb.vncserver)

(autoload rfb.cmdserver
          cmd-enqueue-send-request cmd-key-event cmd-pointer-event cmd-cut-text)

(define (vnc-port disp)
  (+ 5900 disp))

(define-values (vnc-server-shutdown! vnc-server-shutdown?)
  (let ((shutdown? #f))
    (values (lambda ()
              (set! shutdown? #t))
            (lambda ()
              shutdown?))))

(define (vnc-initial-handshake title fb in out)
  (define (protocol-version)
    (format out "RFB 003.003\n")
    (read-line in)
    (flush out))
  (define (authentication)
    (write-u32 1 out 'big-endian)
    (flush out))
  (define (client-initialisation)
    (read-byte in))
  (define (server-initialisation width height
                                 bits-per-pixel depth
                                 big-endian-flag true-colour-flag
                                 red-max green-max blue-max
                                 red-shift green-shift blue-shift
                                 name)
    (pack "nnCCCCnnnCCCxxxN/a"
          (list width height
                bits-per-pixel depth
                big-endian-flag true-colour-flag
                red-max green-max blue-max
                red-shift green-shift blue-shift
                name)
          :output out)
    (flush out))
  (protocol-version)
  (authentication)
  (client-initialisation)
  (server-initialisation (width-of fb) (height-of fb)
                         32 24 1 1 255 255 255 0 8 16 title))

(define (vnc-set-pixel-format servant
                              bits-per-pixel depth
                              big-endian-flag true-color-flag
                              red-max green-max blue-max
                              red-shift green-shift blue-shift)
  (define shift-alist '((1 . 7) (3 . 6) (7 . 5) (15 . 4)
                        (31 . 3) (63 . 2) (127 . 1) (255 . 0)))
  (define (mask m s)
    (ash m (+ s (cdr (assq m shift-alist)))))
  (define (shift s s0 m)
    (- (+ s s0) (cdr (assq m shift-alist))))
  (let ((vnc-color (let ((red-mask (mask red-max 16))
                         (green-mask (mask green-max 8))
                         (blue-mask (mask blue-max 0))
                         (red-shift (shift red-shift -16 red-max))
                         (green-shift (shift green-shift -8 green-max))
                         (blue-shift (shift blue-shift 0 blue-max)))
                     (lambda (v)
                       (+ (ash (logand v red-mask) red-shift)
                          (ash (logand v green-mask) green-shift)
                          (ash (logand v blue-mask) blue-shift)))))
        (endian (if (= big-endian-flag 0)
                    'little-endian
                    'big-endian)))
    (slot-set! servant 'write-pixel (lambda (c out)
                                      ((case bits-per-pixel
                                         ((32) write-u32)
                                         ((16) write-u16)
                                         ((8) write-u8))
                                       (vnc-color c) out endian))))
;;   (let ((fb (slot-ref servant 'frame-buffer)))
;;     (vnc-frame-buffer-update-request servant 0 0 0 (width-of fb) (height-of fb)))
  )

(define (vnc-fix-colour-map-entries servant first-colour . colours)
  ;DO NOTHING
  #f)

(define (vnc-set-encodings servant . encodings)
  ;TODO need implements
  #f)

(define (vnc-frame-buffer-update-request servant incremental
                                         x-position y-position width height)
  (slot-set! servant 'update-request? #t)
  (enqueue-vnc-frame-buffer-update servant incremental
                                   x-position y-position width height))

(define (enqueue-vnc-frame-buffer-update servant incremental
                                         x-position y-position width height)
  (servant-enqueue-send-request servant 
                                (lambda ()
                                  (vnc-frame-buffer-update servant incremental
                                                           x-position y-position
                                                           width height))))

(define (vnc-key-event servant down-flag key)
  (cmd-enqueue-send-request (cut cmd-key-event (servant-id servant) down-flag key <>)))

(define (vnc-pointer-event servant button-mask x-position y-position)
  (cmd-enqueue-send-request
   (cut cmd-pointer-event (servant-id servant) button-mask x-position y-position <>)))

(define (vnc-client-cut-text servant text)
  (cmd-enqueue-send-request (cut cmd-cut-text (servant-id servant) text <>)))

(define (vnc-frame-buffer-update servant incremental
                                 x-position y-position width height)
  (let* ((fb (slot-ref servant 'frame-buffer))
         (write-pixel (slot-ref servant 'write-pixel))
         (out (slot-ref servant 'out)))
    (when write-pixel
      (let ((tiles (if (= incremental 1)
                       (fb-updated-tile-list fb (slot-ref servant 'last-update-time))
                       (fb-all-tiles fb))))
        (unless (= (size-of tiles) 0)
          (write-u8 0 out)
          (write-u8 0 out)
          (write-u16 (size-of tiles) out 'big-endian)
          (write-encoding write-pixel tiles out)
          (slot-set! servant 'update-request? #f))
        (slot-set! servant 'last-update-time (sys-time))))))

(define (write-encoding write-pixel tiles out)
  (map (lambda (tile)
         (if (tile-rre-mode? tile)
             (write-rre-encoding write-pixel tile out)
             (write-raw-encoding write-pixel tile out)))
       tiles))
  
(define (write-raw-encoding write-pixel tile out)
  (write-u16 (x-of tile) out 'big-endian)
  (write-u16 (y-of tile) out 'big-endian)
  (write-u16 (width-of tile) out 'big-endian)
  (write-u16 (height-of tile) out 'big-endian)
  (write-s32 0 out 'big-endian)
  (for-each (lambda (c)
              (write-pixel c out))
            (buffer-of tile)))

(define (write-rre-encoding write-pixel tile out)
  (write-u16 (x-of tile) out 'big-endian)
  (write-u16 (y-of tile) out 'big-endian)
  (write-u16 (width-of tile) out 'big-endian)
  (write-u16 (height-of tile) out 'big-endian)
  (write-s32 2 out 'big-endian)
  (write-u32 (tile-num-of-boxes tile) out 'big-endian)
  (write-pixel (background-color tile) out)
  (for-each (lambda (box)
              (write-pixel (list-ref box 4) out)
              (write-u16 (list-ref box 0) out 'big-endian)
              (write-u16 (list-ref box 1) out 'big-endian)
              (write-u16 (list-ref box 2) out 'big-endian)
              (write-u16 (list-ref box 3) out 'big-endian))
            (tile-box-list tile)))

(define-values (get-all-servants servant-add! servant-delete! servant-alive?)
  (let ((servant-list '()))
    (values (lambda ()
              servant-list)
            (lambda (servant)
              (push! servant-list servant))
            (lambda (servant)
              (set! servant-list (remove (cut eq? servant <>) servant-list))
              (if (and (= (length servant-list) 0) (vnc-server-shutdown?))
                  (exit 0)))
            (lambda (servant)
              (memq servant servant-list)))))

(define (send-update-request-all-servants)
  (for-each (lambda (servant)
              (let ((fb (slot-ref servant 'frame-buffer)))
                (when (slot-ref servant 'update-request?)
                  (enqueue-vnc-frame-buffer-update servant 1 0 0
                                                   (width-of fb)
                                                   (height-of fb)))))
            (get-all-servants)))

(define-class <servant> ()
  ((id)
   (in :init-keyword :in)
   (out :init-keyword :out)
   (selector :init-keyword :selector)
   (frame-buffer :init-keyword :frame-buffer)
   (last-update-time :init-value 0)
   (write-pixel :init-value #f)
   (send-queue :init-form (make-queue))
   (update-request? :init-value #f)))

(define make-servant
  (let ((counter 0))
    (lambda (selector title fb in out)
      (guard (e ((<system-error> e)
                 #f))
             (vnc-initial-handshake title fb in out))
      (let ((servant (make <servant>
                       :frame-buffer fb :selector selector :in in :out out)))
        (slot-set! servant 'id counter)
        (inc! counter)
        (vnc-set-pixel-format servant 32 32 1 1 255 255 255 0 8 16)
        (servant-add! servant)
        (selector-add! selector in
                       (lambda (port flag)
                         (servant-enqueue-recv-request servant))
                       '(r))))))

(define (servant-id servant)
  (slot-ref servant 'id))

(define (servant-recv-data servant)
  (let ((in (slot-ref servant 'in)))
    (with-port-locking in
      (lambda ()
        (when (and (servant-alive? servant)
                   (byte-ready? in))
          (receive (proc args) (servant-recv-proc&args servant)
            (apply proc servant args))
          (servant-enqueue-recv-request servant))))))

(define (servant-enqueue-recv-request servant)
  (recv-enqueue! (cut servant-recv-data servant)))

(define (servant-terminate! servant)
  (receive (selector in out) (apply values (map (cut slot-ref servant <>)
                                                '(selector in out)))
    (selector-delete! selector in #f #f)
    (selector-delete! selector out #f #f)
    (servant-delete! servant)))

(define (servant-need-frame-buffer-update? servant)
  (<= (slot-ref servant 'last-update-time)
      (fb-last-update-time (slot-ref servant 'frame-buffer))))

(define servant-recv-proc&args
  (let ((request-table `((0 . (,vnc-set-pixel-format
                               . ,(lambda (in)
                                    (unpack "xxxCCCCnnnCCCxxx" :input in))))
                         (1 . (,vnc-fix-colour-map-entries
                               . ,(lambda (in)
                                    (unpack "xnn/(nnn)" :input in))))
                         (2 . (,vnc-set-encodings
                               . ,(lambda (in)
                                    (unpack "xn/N" :input in))))
                         (3 . (,vnc-frame-buffer-update-request
                               . ,(lambda (in)
                                    (list (read-byte in)
                                          (read-u16 in 'big-endian)
                                          (read-u16 in 'big-endian)
                                          (read-u16 in 'big-endian)
                                          (read-u16 in 'big-endian)))))
                         (4 . (,vnc-key-event
                               . ,(lambda (in)
                                    (let ((down-flag #f) (key #f))
                                      (set! down-flag (read-byte in))
                                      (read-byte in)
                                      (read-byte in)
                                      (set! key (read-u32 in 'big-endian))
                                      (list down-flag key)))))
                         (5 . (,vnc-pointer-event
                               . ,(lambda (in)
                                    (list (read-byte in)
                                          (read-u16 in 'big-endian)
                                          (read-u16 in 'big-endian)))))
                         (6 . (,vnc-client-cut-text
                               . ,(lambda (in)
                                    (unpack "xxxN/a" :input in)))))))
    (lambda (servant)
      (guard (e ((<system-error> e)
                 (values (lambda _
                           (servant-terminate! servant))
                         '())))
             (let ((in (slot-ref servant 'in)))
               (let ((msg-type (read-byte in)))
                 (cond
                  ((eof-object? msg-type)
                   (values (lambda _
                             (servant-terminate! servant))
                           '()))
                  ((assq msg-type request-table)
                   => (lambda (lst)
                        (values (cadr lst) ((cddr lst) in))))
                  (else
                   (values (lambda _ #f) '())))))))))
  
(define (servant-send-data servant)
  (let ((selector (slot-ref servant 'selector))
        (out (slot-ref servant 'out))
        (queue (slot-ref servant 'send-queue)))
    (cond
     ((queue-empty? queue)
      (selector-delete! selector out #f #f))
     (else
      (let ((send-request-proc (dequeue! queue)))
        (guard (e ((<system-error> e)
                   (servant-terminate! servant)))
               (send-request-proc)
               (flush out)))))))

(define (servant-enqueue-send-request servant send-request-proc)
  (let ((queue (slot-ref servant 'send-queue))
        (selector (slot-ref servant 'selector))
        (out (slot-ref servant 'out)))
    (when (queue-empty? queue)
      (selector-add! selector out
                     (lambda (port flag)
                       (with-port-locking port
                         (lambda ()
                           (servant-send-data servant))))
                     '(w)))
    (enqueue! queue send-request-proc)))

(define (vnc-server selector title vnc-port-num fb)
  (let ((sock (make-server-socket 'inet vnc-port-num :reuse-addr? #t)))
    (selector-add! selector (socket-fd sock)
                   (lambda _
                     (let* ((client (socket-accept sock))
                            (vnc-in (socket-input-port client :buffered? #t))
                            (vnc-out (socket-output-port client)))
                       (make-servant selector title fb vnc-in vnc-out)))
                   '(r))))

(provide "rfb/vncserver")

;; end of file
