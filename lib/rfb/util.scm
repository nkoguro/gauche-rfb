;; -*- coding: utf-8; mode: scheme -*-
;;
;; util.scm - Some utilities
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;
(define-module rfb.util
  (export with-slots
          calc-index)
  )

(select-module rfb.util)

(define-syntax with-slots
  (syntax-rules ()
    ((_ () obj thunk)
     (thunk))
    ((_ (((var) slot) rest ...) obj thunk)
     (let ((var (getter-with-setter (lambda ()
                                      (slot-ref obj 'slot))
                                    (lambda (v)
                                      (slot-set! obj 'slot v)))))
       (with-slots (rest ...) obj thunk)))
    ((_ ((var slot) rest ...) obj thunk)
     (let ((var (slot-ref obj 'slot)))
       (with-slots (rest ...) obj thunk)))
    ((_ ((var) rest ...) obj thunk)
     (with-slots (((var) var) rest ...) obj thunk))
    ((_ (var rest ...) obj thunk)
     (with-slots ((var var) rest ...) obj thunk))))

(define (calc-index x y w)
  (+ x (* y w)))

     
(provide "rfb/util")


;; end of file
